import java.util.Scanner;


public class FileMethod {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print(" enter salary: ");
        double salary = in.nextDouble();
        System.out.print(" enter hours: ");
        double hours = in.nextDouble();


        if (hours > 60) {
            System.out.println("Работник не может работать больше 60 часов");
            System.exit(0);
        } else {
            if (hours > 40) {
                hours = 40 + (hours - 40) * 1.5;
            }

            if (salary < 8) {
                System.out.println("Работник не может получать меньше 8 долдаров");
                System.exit(0);
            } else {
                double result = salary * hours;
                System.out.println(result);
            }
        }
    }
}